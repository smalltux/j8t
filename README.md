# See also

[j4t](https://gitlab.com/smalltux/j4t) requires just six unique characters to convert a JavaScript program of your choice, while maintaining strict compliance with the ES10 (ES2019) specification.

[j5t](https://gitlab.com/smalltux/j5t) requires seven unique characters to convert a JavaScript program, while maintaining strict compliance with the ES10 (ES2019) specification. "+" and parentheses are not used.

# Overview

j8t, also known as `({}+[])[!![]+!![]+!![]]+(!![]+!![]+!![]+!![]+!![]+!![]+!![]+!![]+[])+(!![]+[])[+[]]` or `({}+[])[[]+(+([]<{}))+(+[])]+(+(([]<{})+([]<{})+([]<{})+([]<{})+([]<{})+([]<{})+([]<{})+([]<{}))+[])+(([]<{})+[])[+[]]`.

Implemented in 2018 - 2023 by David Vuorio. It is inspired by [hieroglyphy](https://github.com/alcuadrado/hieroglyphy) and similar projects. Designed for compatibility with ES5 and up.

Note that the translated program that is generated is equivalent to using `Function(<user code>)`. Therefore, only code that works with that invocation will work. In particular:

- In Node, `require` will not work as it's not accessible when using Function to execute code.
- `import` and `export` will not work in any environment.
- There could possibly be differences in scoping of top-level variables and functions (should not be a limiting factor in practice except perhaps for some edge cases).

Creating classical modules is still possible by placing them on the global object.

It's possible to work around some of these restrictions by using the questionable `eval` function, but even then there are potential barriers.

Note: the code in this branch is *not* fully optimized in terms of output size. See e.g. the branch `unforeseen-optimizations` for some more extreme optimizations.

The key differences to other projects are: (not all apply to all projects)

- **10 unique characters are used (may be improved to 9 later).** This gives some room to make significant improvements. Similar converters that I know of and that use fewer characters all appear to rely on non-standardized behavior, deprecated methods or other hacks, or only support small programs (see below). Clearly this must be addressed!
- **j8t can convert itself.** See below.
- **Optimized output size.** By using a bootstrapping up front, the overall converted program is much smaller than it would be otherwise, even for hello world. Also, not doing this would most likely lead to a stack error or similar, even for relatively small programs. See below for current expected output size.
- **Reliance on well-specified behavior.**
  - For instance, other converters make assumptions about what the string representation of a function looks like. However, the ES5 and ES6 specs state: "[T]he use and placement of white space, line terminators, and semicolons within the representation String is implementation-dependent." So in theory, this cannot be relied on.
  - The implementation is designed for strict adherence to ES5+.
- **Features that are deprecated or listed in Annex B are avoided.** `escape` and `unescape` are listed in Annex B and are therefore not used.
- **The output size can be predicted with byte precision.** See below.
- **No dependence on environment-specific behaviour.** Only generic JavaScript code is used.

# Screenshot

- Wrapped at 191 columns for display purposes only. Newlines are not used in the output.
- The vast majority of what's in the screenshot is generic logic, which serves to translate the user code.
- User code: `console.log("Hello, world!");`
- Hex: `63 6f 6e 73 6f 6c 65 2e 6c 6f 67 28 22 48 65 6c 6c 6f 2c 20 77 6f 72 6c 64 21 22 29 3b 0a`
- Encoding: `"{+}})]]}{[}})]][{!(()!][{{}}{[]+!+[[[![]{{+}}!])!{+(+!}){{})!!][{!+(+[[(!(({][!("`
  - This can be seen at the bottom.

![Hello world, converted by j8t](hello.png "Hello world, converted by j8t")

# Using j8t

Run `node j8t.js` for usage info.

The output size can currently be calculated as follows:

- 0 bytes if there are 0 bytes in the input.
- Otherwise, `7369 + 2.67 * [number of bytes in the input]` bytes, approximately. See the code for the exact rule.

By running the input through a minifier first, the output could potentially be made smaller than the input for some programs. No minification is done by j8t though.

# Questions

**Why?**  
See next question.

**Why not?**  
Good question!

**Can it convert itself?**  
Of course! Run `node j8t.js` for details.

**Should I use this in production?**  
No.

**Is 9 characters possible while achieving the same goals?**  
There's at least one possibility, although there is some potential ambiguity when considering the letter of the ES5 specification (and later specifications). That aside, the answer is yes. A proof of concept will be published here at some point (TM).

**Is 8 characters or less possible while achieving the same goals?**  
Perhaps, but it's unclear at the moment what that would look like. I'm not aware of a current solution that can do this.
