"use strict";

// In this version, ES5 support "should" work, however it has not been tested.
// Unlike some previous versions, no hacks relying on strict mode or the like are used here, though
// there may be some accidental ES5 syntax used in places. If this code does not work in ES5, it is likely
// that only minor tweaks are needed.
//
// In order to make the code (somewhat) readable, there are numerous help functions that massage
// more verbose code into something more practical for translation. For instance, consider:
//
// const _fromCharCode = callFunction(trimAndTranslateString(renameVariables(FROMCHARCODE_RENAME_MAPPING, ...)));
//
// 1) "renameVariables" does a search-replace to replace something like _bit_counter with something
//    that can be translated more efficiently.
// 2) trimAndTranslateString:
//    2a) trimString: removes all whitespace and newlines. Explicit spaces can be inserted with
//        /*space*/ . After that, block comments and line comments are removed.
//        Note that trimString makes some assumptions about the input, and thus is not safe to use on arbitrary code.
//    2b) translateString: half-naive function which translates one character at a time and concatenates the
//        result together. E.g. "NaN" would be translated as "NaN"[0] + "NaN"[1] + "NaN"[0] or something like:
//        (+{}+[])[+[]]+(+{}+[])[+!![]]+(+{}+[])[+[]]
// 3) callFunction: invokes Function(<code>)() so the code is executed.
//
// To ensure high efficiency in encoding the user code, the following steps are taken, not really in this order:
// 1) Get the user code as a UTF-8 byte sequence.
// 2) Pack the byte sequence into bits of three, and encode using the eight characters in LITERAL_8_ALPHABET.
//    Wrap with LITERAL_QUOTE, defined below.
// 3) This new sequence will be decoded in several steps when the encoded program is executed:
//    a) The JS runtime decodes the translation program itself, and executes the "core", which gets as parameters
//       some stuff it can use to access helper functions like String.fromCharCode.
//    b) The "core" decodes a "wrapper" function, which is stored in a less efficient manner (7x the original size).
//    c) The "wrapper" function then decodes the actual user code, and runs it.
//
// There are a number of possible alphabets. For instance, "<" is used instead of "!" in a previous version.
// Note: some earlier commits like that one, while in practice they should work fine in at least ES6, make some
// assumptions about a JS runtime that do not necessarily hold, such as about strict mode. See for instance 33d2214 for
// some details.
//
// Other options for that char: (with true and false, and 1 and 0 derivable from a single char, the current code can
// be adapted)
//
// +-------+---------+--------+----------------------+
// | Token | true    | false  | 0/1 (from bit)       |
// +-------+---------+--------+----------------------+
// |   !   |  !![]   |  ![]   | 0: +!(+(+[]+[])+1)   |
// |       |         |        | 1: +!(+(![]+[])+1)   |
// |       |         |        |         ^^^^^^       |
// |       |         |        |                      |
// |   =   | []==+[] | []==[] | 0: +([][[]]+[]==+[]) |
// |       |         |        | 1: +([][[]]=[]==+[]) |
// |       |         |        |      ^^^^^^^^^       |
// |       |         |        |                      |
// |   <   |  []<{}  | []<[]  | 0: +([]<[]+([]+[]))  |
// |       |         |        | 1: +([]<[]+([]<[]))  |
// |       |         |        |             ^^^^^    |
// |       |         |        |                      |
// |   >   | ({}>[]) | []>[]  | 0: +([]+([]+[])>[])  |
// |       |         |        | 1: +([]+([]>[])>[])  |
// |       |         |        |          ^^^^^       |
// +-------+---------+--------+----------------------+
//
// This is just to show what is possible, and for practical reasons severe restrictions are placed on the part that is
// to be evaluated with Function (marked with ^^^).
//
// I haven't found the original reference where I saw the underlying [][] idea (for the "=" case), but it is mentioned
// here for instance: https://stackoverflow.com/questions/7202157/why-does-return-the-string-10 . It's been adapted a
// bit here.

const LITERAL_8_ALPHABET = "+!({[]})";

// Quotes are used in order to obtain backslashes (see below), and to store things like user code efficiently.
// Apostrophe is also possible, as is backtick if ES6 is allowed. Some optimizations are however disabled in this case.
const LITERAL_QUOTE = '"';

let _chars = [];

// Backslash could be used to escape quotes, which might optimize things further. This feature is not exploited at the
// moment. For now, it is used only to get at \uNNNN , which makes it possible to obtain some characters that would
// otherwise be more difficult to manufacture.

/* Define all atoms first */

defineChar(`${LITERAL_QUOTE}`, `${LITERAL_QUOTE}\\${LITERAL_QUOTE + LITERAL_QUOTE}`); // E.g. "\"" in the output
defineChar('\\', `${LITERAL_QUOTE}\\\\${LITERAL_QUOTE}`); // E.g. "\\" in the output

const _empty_array = "[]";
const _undefined = _empty_array + "[[]]"; // Lookup property "" of the empty array
const _false = "![]"; // [] is truthy by definition, so ![] is false
const _true = "!![]";
const _object = "{}";
const _NaN = numberify(_object); // Alternatively: +[][[]]

/* Now the simple molecules, using only atoms */

range(0, 9).map((i) => defineChar(i, getNumber(i)));

defineCharsFromObj("undefined", _undefined);
defineCharsFromObj("false", _false);
defineCharsFromObj("true", _true);
defineCharsFromObj("NaN", _NaN);

// This yields "c" and "o". They are needed in order to get "constructor", which is needed to get Function.
// Some other characters in this string are also used, but they could be encoded if needed.
defineCharsFromObj("[object Object]", _object);

/* Now the complex molecules, relying on other molecules */

const _mCh_str = callFunction(trimAndTranslateString(`
    return ${LITERAL_QUOTE}\\u006d\\u0043\\u0068${LITERAL_QUOTE}
`));

/* Core bootstrapping logic in this section. See eightify etc. below for the wrapper bits. */

function translateProgram(buf) {
    console.warn("Generating output...");

    let res = "";

    if (buf.length) {
        res = translateProgramUsingBootstrap(buf);
    } else {
        console.warn();
        console.warn('Bootstrapping length: 0 bytes');
        console.warn('  whereof core logic: 0 bytes');
        console.warn('  whereof "mCh"     : 0 bytes');
        console.warn('  whereof wrapper   : 0 bytes');
        console.warn('Payload (user code) : 0 bytes (approx. 0 * 8/3 bytes)');
        console.warn('Total length (UTF-8): 0 bytes');
    }

    console.warn();
    console.warn("Code generation completed.");

    return res;
}

function translateProgramUsingBootstrap(buf) {
    let res = "";

    // Generate some "bytecode" in a long string. This should help keep the output size and stack size down
    // (relatively speaking), and makes it possible for the program to translate itself as well.
    // The following tokens are kept completely at bay as they are very expensive to translate:
    //
    // +--------+---------------------------------------------------------------+
    // | Token  | Technique used - note that only some are generic replacements |
    // +--------+---------------------------------------------------------------+
    // |   =    | Wrap with function() to emulate assignment                    |
    // | ==/=== | Handled in other ways, e.g. with "if (truthy)" and similar    |
    // |   ,    | Wrap with function() to emulate several function parameters   |
    // |   ;    | { <code> }                                                    |
    // |   /    | Not needed anymore / solved by the other techniques           |
    // |   &    | +[] / ![] can help us get the lowest "bit" of any             |
    // |        | character, when only + and ! are allowed (not the *actual*    |
    // |        | bit, but makes it possible to distinguish one from the other) |
    // |   .    | [<property>] instead of .property                             |
    // |   %    | Check if e.g. _seven_chars[++x] is truthy, instead of x % 16  |
    // |   -    | Not needed.                                                   |
    // +-------+----------------------------------------------------------------+

    const HELPER_FUNCTION_RENAME_MAPPING = {
        _empty_array: "[]",
        _empty_string: "([]+[])",
        _true: _true,
        _false: _false,

        _seven_chars: `([]+(+[])+(+[])+_false)`,

        _lbrb: translateString("[]", true),

        _join_str: translateString("join", true),
        _constructor_str: translateString("constructor", true),
        _return_str: translateString("return", true),

        _fro_str: translateString("fro", true),
        _ar_str: translateString("ar", true),
        _ode_str: translateString("ode", true),
        _fromCharCode_str: `_fro_str + _mCh_str + _ar_str + _mCh_str[1] + _ode_str`,

        _uns_str: translateString("uns", true),
        _ift_str: translateString("ift", true),
        _unshift_str: `_uns_str + _mCh_str[2] + _ift_str`,

        _Function: `_wrapper_function[_constructor_str]`,

        _character_code             : "N",
        _wrapper_code_index         : "f",
        _mCh_str                    : "t",
        _result_array               : "o",
        _wrapper_code_str           : "a",
        _wrapper_function           : "u",
        _bit_counter                : "b",
        _user_code_str              : "r",
        _recursive_character_builder: "j"
    };

    // The two possible routes that are applied in _get_bit go like this:
    //
    // +---------------+---------+-----------+-----------------------------------------------+
    // | Step          | Case A  | Case B    | Comment                                       |
    // +---------------+---------+-----------+-----------------------------------------------|
    // | Input bit     |    0    |     1     |                                               |
    // | Input char    |   '+'   |    '!'    |                                               |
    // | +[] / ![]     |    0    |   false   | Both are falsy, not good, more work is needed |
    // | stringify     |   '0'   |  'false'  |                                               |
    // | numberify     |    0    |    NaN    |                                               |
    // | +1            |    1    |    NaN    | Truthy and falsy                              |
    // | negate        |  false  |   true    | Same truth value as input bit                 |
    // | numberify     |    0    |     1     |                                               |
    // +---------------+---------+-----------+-----------------------------------------------+

    HELPER_FUNCTION_RENAME_MAPPING._get_bit = `
        +!(+(_Function(_return_str + _wrapper_code_str[_wrapper_code_index++] + _lbrb)() + []) + 1)
    `;

    let helper_function = renameVariables(HELPER_FUNCTION_RENAME_MAPPING, `
        return (function(_wrapper_code_str) {
        return (function(_mCh_str) {
        return (function(_user_code_str) {
            (function(_result_array) { // Wrap to get _result_array = []
                (function /*space*/ _wrapper_function(_wrapper_code_index) { // Wrap to get _wrapper_code_index = 0
                    if (_wrapper_code_str[_wrapper_code_index]) {
                        // Wrap to get _bit_counter = 0 and _character_code = 0
                        (function(_bit_counter) {
                            (function /*space*/ _recursive_character_builder(_character_code) {
                                // The wrapper is encoded as seven-bit ASCII, without storing an eighth bit.
                                // Thus, build up _character_code using seven bits, from "+" and "!".
                                // This means the return statement has to trigger seven times.
                                // As _bit_counter++ will have values 0, 1, ..., 7, and the highest
                                // index so to speak in _seven_chars is 6, that's what'll happen.
                                // Note that _bit_counter is only used as a counter. _wrapper_code_index
                                // keeps track of where to look in the input string.
                                if (_seven_chars[_bit_counter++]) {
                                    return (_recursive_character_builder(
                                        // Equivalent to: _character_code = _character_code * 2 + bit
                                        //   where bit is 0 (for "+") or 1 (for "!")
                                        _character_code + _character_code + (_get_bit)
                                    ))
                                }

                                // Use unshift instead of push as that's one less "special" character to translate.
                                // _result_array.unshift(String.fromCharCode(_character_code))
                                _result_array[_unshift_str](
                                    _empty_string[_constructor_str][_fromCharCode_str](
                                        _character_code
                                    )
                                )
                            })(+[]) // Execute _recursive_character_builder with _character_code = 0
                        })([]) // Execute anonymous function with _bit_counter = 0

                        // Proceed to the next group of bits in the wrapper code (corresponds to one character).
                        + _wrapper_function(_wrapper_code_index)
                    } else {
                        // The entire wrapper code is now in _result_array.

                        _Function(
                            _result_array
                            [_join_str]([]) // Turn the array of characters into a string ([] gets coerced into "")
                        )() // Get the wrapper into "standby" mode ("return function(input) { ... }" was executed here)
                        (_user_code_str) // Execute the user code by passing it into the wrapper
                    }
                })(+[]) // Execute anonymous function with _wrapper_code_index = 0
            })([]) // Execute anonymous function with _result_array = []
        }) // Returns a function that takes the third argument below, see _user_code_str above.
        }) // Returns a function that takes the second argument below, see _mCh_str above.
        }) // This returns a function that takes the first argument below. See _wrapper_code_str above.
    `);

    const helper_function_trimmed = trimString(helper_function);
    const payload_input = trimString(getUneightify());

    let bitified_input = "";

    for (let outer = 0; outer < payload_input.length; ++outer) {
        let code = payload_input.charCodeAt(outer);
        let s = "";

        for (let i = 0; i <= 6; ++i) {
            const bit = code & 1;

            if (bit === 0) {
                s = "+" + s;
            } else {
                s = "!" + s;
            }

            code >>= 1;
        }

        bitified_input = s + bitified_input; // Add backwards to allow using unshift instead of push
    }

    const wrapper_code = LITERAL_QUOTE + bitified_input + LITERAL_QUOTE;
    const compressed_mCh_str = compress(_mCh_str);
    const user_code = LITERAL_QUOTE + eightify(buf) + LITERAL_QUOTE;

    res = compress(callFunction(translateString(helper_function_trimmed))) +
        `(${wrapper_code})` +
        `(${compressed_mCh_str})` +
        `(${user_code})`
    ;

    const bootstrapping_length = res.length - (user_code.length - 2);
    const glue_length = bootstrapping_length - compressed_mCh_str.length - bitified_input.length;

    console.warn();
    console.warn(`Bootstrapping length: ${bootstrapping_length} bytes`);
    console.warn(`  whereof core logic: ${glue_length} bytes`);
    console.warn(`  whereof "mCh"     : ${compressed_mCh_str.length} bytes`);
    console.warn(`  whereof wrapper   : ${bitified_input.length} bytes (${payload_input.length} * 7 bytes)`);
    console.warn(`Payload (user code) : ${user_code.length - 2} bytes (approx. ${buf.length} * 8/3 bytes)`);
    console.warn(`Total length (UTF-8): ${res.length} bytes`);

    return res;
}

/* In this section there are mostly high level and generic functions */

function renameVariables(mapping, str) {
    let res = str;
    let old_res;

    do {
        old_res = res;
        for (const key in mapping) {
            res = res.replace(RegExp(key, "g"), mapping[key]);
        }
    } while (old_res !== res);

    return res;
}

function trimString(input) {
    let res = input
            .replace(/\/\/.*/g, "") // remove line comments
            .replace(/\n/g, "")
            .replace(/\s+/g, "")
            .replace(/..space../g, " ") // replace /*space*/
            .replace(RegExp("/\\*[^*]*\\*/", "g"), "") // remove block comments
    ;

    return res;
}

function compress(input) {
    let res = input;

    while ([] < {}) {
        const old_length = res.length;
        res = compress_once_1(res);
        res = compress_once_2(res);

        if (res.length === old_length) {
            break;
        }
    }

    return res;
}

function compress_once_1(input) {
    // Replace ("a")+("b")...+("z") with ("ab...z"), where a, b, ..., z are any characters
    // For practical reasons, does nothing right now if LITERAL_QUOTE is changed from its default value.
    let res = input;

    const UNIT_LENGTH = '("+")'.length;
    const UNIT_PLUS_ONE_LENGTH = UNIT_LENGTH + 1;

    while (+[] + []) {
        const regex = RegExp('(\\("."\\)\\+)+\\("."\\)', "g");
        const match = regex.exec(res);

        if (match !== null) {
             const match_str = match[0];
             const index = match.index;
             let middle = '("';

             for (let i = 0; i < match_str.length / UNIT_PLUS_ONE_LENGTH; ++i) {
                 const ch = match_str[i * UNIT_PLUS_ONE_LENGTH + 2];
                 middle += ch;
             }

             middle += '")';

             res = res.substring(0, index) + middle + res.substring(index + match_str.length);
        } else {
             break;
        }
    }

    return res;
}

function compress_once_2(input) {
    // Replace all occurrences of +("x") with +"x" , where x is any character.
    // For practical reasons, it does nothing right now if LITERAL_QUOTE is changed from its default value.
    // For the same reasons, it does not optimize for +("foobar") as there is more than one character in the middle.
    return input.replace(/\+\("(.)"\)/g, '+"$1"');
}

function getFromAlphabet(c) {
    let res = undefined;
    const index = LITERAL_8_ALPHABET.indexOf(c);
    if (index >= 0) {
        res = LITERAL_8_ALPHABET[index];
    }
    return res;
}

function concat(...args) {
    let res = "(";
    if (args[0][0] !== "[" && args[0][0] !== "(") {
        res += "[]+"; // Coerce into string
    }
    for (let i = 0; i < args.length; ++i) {
        if (i) {
            res += "+";
        }
        if (args[i][0] !== "[" && args[i][0] !== "(") {
            res += "(" + args[i] + ")";
        } else {
            res += args[i];
        }
    };
    res += ")";
    return res;
}

function concatArray(arr) {
    return concat.apply(undefined, arr);
}

// The name is misleading - any number higher than 9 will be returned as an expression that evaluates to a string,
// not a number.
function getNumber(num) {
    let res = "";

    if (num === 0) {
        res = numberify(_empty_array);
    } else if (num === 1) {
        res += numberify(_true);
    } else if (num >= 2 && num <= 9) {
        res += _true;
        range(2, num).forEach((_) => {
            res += numberify(_true);
        });
    } else {
        const orig_digits = num.toString().split("");
        const translated_digits = orig_digits.map((c) => stringify(getNumber(Number(c))));
        res = concatArray(translated_digits);
    }

    return res;
}

function range(from, to) {
    let res = [];
    for (let i = from; i <= to; ++i) {
        res.push(i);
    }
    return res;
}

function numberify(obj) {
    return "+" + obj;
}

function stringify(obj) {
    return "(" + obj + "+" + _empty_array + ")";
}

function defineChar(c, val) {
    if (!_chars[c] || val.length < _chars[c].length) {
        _chars[c] = val;
    }
}

function defineCharsFromObj(chars_to_set, obj) {
    for (let i = 0; i < chars_to_set.length; ++i) {
        defineChar(chars_to_set[i], getCharFrom(obj, i));
    }
}

function quote(c) {
    return LITERAL_QUOTE + c + LITERAL_QUOTE;
}

function getChar(c, disable_quoting) {
    let res = "";
    let core_alphabet_char = getFromAlphabet(c);

    if (disable_quoting !== true && core_alphabet_char !== undefined && core_alphabet_char !== LITERAL_QUOTE) {
        res = quote(core_alphabet_char);
    } else if (_chars[c]) {
        res = _chars[c];
    }

    if (disable_quoting === true && res.indexOf(LITERAL_QUOTE) >= 0) {
        throw Error("Couldn't get a non-quoted option!");
    }
    
    return res;
}

function getCharFrom(orig_obj, idx) {
    // The counter-intuitive padding optimization used here is inspired by jscrewit.
    // It's a naive approach which could be optimized for speed, but that doesn't seem necessary
    // as the number of calls to this function is fixed and small enough.
    // Find the shortest representation by trying a number of options.
    // Consider for instance looking up "c":
    //
    // - "[object Object]"[5]
    // - "false[object Object]"[10]
    //
    // While there is some extra work to add the "false" bit in, translating 10
    // can be done more efficiently than translating 5, as 10 is calculated with "1" + "0",
    // whereas 5 is calculated as 1 + 1 + 1 + 1 + 1.

    // Ensure +obj will work. E.g. ++{} would not work.
    const obj = ensureParensOrBrackets(orig_obj);

    // In this context, only the following are possible:
    // boolean, number, object, undefined
    const type = typeof Function("return " + obj)();

    // Only object guarantees to yield a string when adding the prefixes below.
    const should_stringify_obj_when_adding = type !== "object";
    const tostr_suffix = should_stringify_obj_when_adding ? "+[]" : "";

    const variants = [];

    variants.push(stringify(orig_obj) + "[" + getNumber(idx) + "]");

    // Prefix: 0
    variants.push("(+[]+" + obj + ")[" + getNumber(idx + 1) + "]");

    // Prefix: NaN
    variants.push(`(+{}${tostr_suffix}+` + obj + ")[" + getNumber(idx + 3) + "]");

    // Prefix: true
    variants.push(`(${_true}${tostr_suffix}+` + obj + ")[" + getNumber(idx + 4) + "]");

    // Prefix: false
    variants.push(`(${_false}${tostr_suffix}+` + obj + ")[" + getNumber(idx + 5) + "]");

    if (idx >= "undefined".length) {
        // Prefix: undefined
        variants.push(`([][[]]${tostr_suffix}+` + obj + ")[" + getNumber(idx + 3) + "]");
    }

    variants.sort(function(a, b) {
        return a.length > b.length ? 1 : -1;
    });

    // Return the shortest match
    return variants[0];
}

function ensureParensOrBrackets(obj) {
    return (obj[0] === "(" || obj[0] === "[") ? obj : `(${obj})`;
}

function trimAndTranslateString(str) {
    return translateString(trimString(str));
}

function translateString(str, disable_quoting) {
    // When disable_quoting is true, a character like [ will not be translated as "[", but
    // as something like ([]+{})[+[]] . This allows the translated string to be re-translated
    // without causing issues. A more advanced setup might work around this issue.

    let terms = getTerms(str, disable_quoting);
    return concatArray(terms);
}

function getTerms(str, disable_quoting) {
    return str.split("").map((c) => getChar(c, disable_quoting));
}

function callFunction(literal_function_body) {
    let res = "";

    const [LB, RB] = ["[", "]"];
    const [LP, RP] = ["(", ")"];
    const SORT = translateString("sort");
    const CONSTRUCTOR = translateString("constructor");

    // This boils down to the equivalent of:
    // []["sort"]["constructor""](<function_body>)()
    // i.e. Function(<function_body>()
    res = [
        LB, RB,
        LB, SORT, RB,
        LB, CONSTRUCTOR, RB,
        LP, literal_function_body, RP,
        LP, RP
    ].join("");

    return res;
}

function translateAndPrintProgram(buf) {
    console.warn();
    console.warn(`Raw input size (UTF-8): ${buf.length}`);
    console.warn();

    const prog = translateProgram(buf);

    console.warn("Writing output...");
    process.stdout.write(prog);
}

/* Eightify / uneightify */

function eightify(buf) {
    // This could be rewritten using a stream approach instead, which should handle large payloads better and minimize
    // memory consumption. That said, when the encoded program is executed, it will decode the entire program into
    // memory in several steps before it can execute it.
    let bytes = getUtf8Bytes(buf);
    let bits = bitifyBytes(bytes);
    let res = eightifyBits(bits).join("");

    return res;
}

function getUtf8Bytes(buf) {
    let res = [];

    for (let i = 0; i < buf.length; ++i) {
        res.push(buf.readUInt8(i));
    }

    return res;
}

function bitifyBytes(bytes) {
    let res = [];

    for (let b of bytes) {
        for (let i = 7; i >= 0; --i) {
            res.push(b & (1 << i) ? 1 : 0);
        }
    }

    return res;
}

function eightifyBits(bits) {
    let res = [];

    // Let's assume the caller does not rely on the parameter being preserved
    while (bits.length % 3 !== 0) {
        bits.push(0);
    }

    const num_whole_chunks = Math.floor(bits.length / 3);

    for (let i = 0; i < num_whole_chunks; ++i) {
        let code = 4 * bits[3*i]
                 + 2 * bits[3*i + 1]
                 +     bits[3*i + 2];

        res.push(LITERAL_8_ALPHABET[code]);
    }

    return res;
}

function getUneightify() {
    // Not the most efficient solution, but avoids having to worry about encoding gotchas.

    const UNEIGHTIFY_RENAME_MAPPING = {
        input: "a",
        bit_buf: "c",
        res: "d",
        _code: "e",
        nomNom: "f",
        chars_out: "g",
        encoded_chars: "h",
        // "i" is taken
        abort: "j",
        // "q" is taken
    };

    let fun_str = renameVariables(UNEIGHTIFY_RENAME_MAPPING,
`
    return /*space*/ function(input) {
        // Regarding variables: take into account that there are two var statements, and there's some hoisting going on.
        // So everything should be contain within this function rather than leaking out into the global object.

        function /*space*/ nomNom()
        {
            // Init: set abort to a falsy value.
            // Loop condition: enter the body if we don't have a character yet (chars_out is falsy) and there is a
            // Number at bit 7 in the buffer, counting from 0 (pretty much "bit_buf.length >= 8").
            for (var /*space*/ chars_out = "", encoded_chars = "", i; !chars_out && bit_buf[i = 7] + 1;) {
                // _code = 0;
                // i = 7; (above)
                // while (i >= 0) { ... }
                for (_code = 0; i + 1;)
                    // code += bit_buf[0] << 7
                    // code += bit_buf[1] << 6
                    // ...
                    // code += bit_buf[7] << 0
                    _code += bit_buf[7 - i] << i--;

                // Throw away the bits we just read.
                bit_buf.splice(0, 8);

                if (_code < 128)
                    // decodeURI is not suitable for these chars as some characters like # (%23) will cause trouble
                    //
                    // 1. chars_out += String.fromCharCode(_code)
                    // 2. set "abort" to a truthy value
                    chars_out = String.fromCharCode(_code);
                else /*space*/
                    try {
                        // Add to encoded_chars until it is a valid encoding of a character.
                        //
                        // 1. encoded_chars += "%hh" where "hh" is the hexadecimal representation of _code
                        // 2. chars_out = decodeURI(encoded_chars) , unless it throws.
                        // 3. If it threw, continue. chars_out is still an empty string in this case.
                        //    If it did not throw, it is a valid sequence, and chars_out now contains a character.
                        chars_out = decodeURI(
                            encoded_chars +=
                                (_code < 16 ? "%0" : "%") +
                                _code.toString(16)
                        )
                    } catch (_) { }
            }

            res += chars_out;

            // Return whether we got a character or not.
            return !!chars_out
        }

        for (var /*space*/ bit_buf = [], res = "", q = 0, _code; input[q];) /* { */
            // Turn the encoded characters into individual bits.
            _code = "${LITERAL_8_ALPHABET}".indexOf(input[q++]),

            // bit_buf.push((_code & 4) ? 1 : 0);
            // bit_buf.push((_code & 2) ? 1 : 0);
            // bit_buf.push((_code & 1) ? 1 : 0);
            //
            // if (bit_buf.length >= 32) {
            //     nomNom();
            // }
            bit_buf.push(
                _code >> 2,
                _code & 2 && 1,
                _code & 1
            ) > 31 && nomNom();
        /* } */

        // Parse the rest of the bit buffer.
        while (nomNom());

        // "res" is the user code, execute it.
        Function(res)()
    }
`);

    return fun_str;
}

/* Main */

if (process.argv.length !== 3) {
    console.warn("Usage: node j8t.js FILE");
    console.warn();
    console.warn("The result is printed to stdout.");
    console.warn("If FILE is - j8t will read from stdin. This allows it to encode itself.");
    process.exit(1);
}

try {
    console.warn("Reading input...");

    const filename = process.argv[2];

    if (filename === "-") {
        // Making things a little more complicated just so the raw input size is available for the statistics.
        let input_buf = Buffer.from([]);
        process.stdin.on('readable', () => {
            const chunk = process.stdin.read();
            if (chunk !== null) {
                input_buf = Buffer.concat([input_buf, chunk]);
            }
        });
        process.stdin.on('end', () => {
            translateAndPrintProgram(input_buf);
        });
    } else {
        let has_require = false;
        try {
            if (require) {
                has_require = true;
            }
        } catch (_) { }

        if (has_require) {
            const fs = require('fs');
            const stat = fs.statSync(filename);
            const input = stat && stat.isFile() && fs.readFileSync(filename);

            if (!Buffer.isBuffer(input)) {
                throw Error(`Not a file: ${filename}`);
            }

            translateAndPrintProgram(input);
        } else {
            console.warn(
                "Could not find 'require', are you perhaps running a converted j8t? ;-)" +
                " Try using the stdin invocation instead!"
            );
        }
    }
} catch (e) {
    console.warn(`Error: ${e.message}`)
    process.exit(1);
}
